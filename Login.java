import java.util.Scanner;

public class Login {
    private static final int MAX_LOGIN_ATTEMPTS = 3;

    public boolean loginUser() {
        Scanner scanner = new Scanner(System.in);
        int loginAttempts = 0;

        while (loginAttempts < MAX_LOGIN_ATTEMPTS) {
            System.out.print("Username: ");
            String username = scanner.nextLine();

            System.out.print("Password: ");
            String password = scanner.nextLine();

            // Proses validasi login
            if (validateLogin(username, password)) {
                return true;
            } else {
                System.out.println("Login gagal. Silakan coba lagi.");
                loginAttempts++;
            }
        }

        return false;
    }

    // Metode untuk validasi login
    private boolean validateLogin(String username, String password) {
        // Proses validasi login sesuai dengan aturan aplikasi
        // Misalnya, periksa kecocokan username dan password dengan data yang ada dalam
        // sistem
        // Mengembalikan true jika login berhasil, false jika gagal
        return username.equals("admin") && password.equals("123456");
    }
}
