import java.util.Scanner;

public class DisneyHotstarApp {
    private static final int MAX_LOGIN_ATTEMPTS = 3;

    public static void main(String[] args) {
        System.out.println("Selamat datang di Disney+ Hotstar!");

        Login login = new Login();
        boolean loginBerhasil = login.loginUser();

        if (loginBerhasil) {
            System.out.println("Login berhasil!");

            Pengguna pengguna = new Pengguna("admin", "123456");
            AcaraTV acaraTV = new AcaraTV("Judul Acara TV", 60, 10);
            Film film = new Film("Judul Film", 120, "Sutradara Film");
            TampilanKonten tampilanKonten = new TampilanKonten();

            tampilanKonten.tambahkanKonten(acaraTV);
            tampilanKonten.tambahkanKonten(film);

            Scanner scanner = new Scanner(System.in);
            int pilihan = 0;

            while (pilihan != 6) {
                showMenu();
                pilihan = scanner.nextInt();
                scanner.nextLine();

                switch (pilihan) {
                    case 1:
                        pengguna.aktifkanBerlangganan();
                        break;
                    case 2:
                        pengguna.nonaktifkanBerlangganan();
                        break;
                    case 3:
                        System.out.println("=== Memutar Konten ===");
                        System.out.println("Pilih konten yang ingin diputar:");
                        tampilanKonten.tampilkanSemuaKonten();
                        System.out.print("Masukkan nomor konten: ");
                        int nomorKonten = scanner.nextInt();
                        scanner.nextLine();
                        if (nomorKonten > 0 && nomorKonten <= tampilanKonten.getDaftarKonten().size()) {
                            Konten konten = tampilanKonten.getDaftarKonten().get(nomorKonten - 1);
                            konten.putar();
                        } else {
                            System.out.println("Nomor konten tidak valid.");
                        }
                        break;
                    case 4:
                        System.out.println("=== Menambahkan Komentar ===");
                        System.out.println("Pilih konten yang ingin ditambahkan komentar:");
                        tampilanKonten.tampilkanSemuaKonten();
                        System.out.print("Masukkan nomor konten: ");
                        int nomorKontenKomentar = scanner.nextInt();
                        scanner.nextLine();
                        if (nomorKontenKomentar > 0 && nomorKontenKomentar <= tampilanKonten.getDaftarKonten().size()) {
                            Konten konten = tampilanKonten.getDaftarKonten().get(nomorKontenKomentar - 1);
                            System.out.print("Masukkan komentar: ");
                            String komentar = scanner.nextLine();
                            tambahkanKomentar(konten, komentar);
                        } else {
                            System.out.println("Nomor konten tidak valid.");
                        }
                        break;
                    case 5:
                        System.out.println("=== Memberi Rating ===");
                        System.out.println("Pilih konten yang ingin diberi rating:");
                        tampilanKonten.tampilkanSemuaKonten();
                        System.out.print("Masukkan nomor konten: ");
                        int nomorKontenRating = scanner.nextInt();
                        scanner.nextLine();
                        if (nomorKontenRating > 0 && nomorKontenRating <= tampilanKonten.getDaftarKonten().size()) {
                            Konten konten = tampilanKonten.getDaftarKonten().get(nomorKontenRating - 1);
                            System.out.print("Masukkan rating (0.0-5.0): ");
                            double rating = scanner.nextDouble();
                            scanner.nextLine();
                            if (rating >= 0.0 && rating <= 5.0) {
                                tambahkanRating(konten, rating);
                            } else {
                                System.out.println("Rating tidak valid.");
                            }
                        } else {
                            System.out.println("Nomor konten tidak valid.");
                        }
                        break;
                    case 6:
                        System.out.println("Terima kasih. Sampai jumpa!");
                        int totalDurasiTontonan = pengguna.hitungTotalDurasiTontonan();
                        System.out.println("Total Durasi Tontonan: " + totalDurasiTontonan + " menit");
                        break;
                        break;
                    default:
                        System.out.println("Pilihan tidak valid.");
                        break;
                }
            }
        } else {
            System.out.println("Login gagal. Anda telah melewati batas maksimal percobaan login.");
        }
    }

    private static void showMenu() {
        System.out.println("Pilihan:");
        System.out.println("1. Aktifkan Berlangganan");
        System.out.println("2. Nonaktifkan Berlangganan");
        System.out.println("3. Memutar Konten");
        System.out.println("4. Tambahkan Komentar");
        System.out.println("5. Beri Rating");
        System.out.println("6. Keluar");
        System.out.print("Masukkan pilihan Anda: ");
    }

    private static void tambahkanKomentar(Konten konten, String komentar) {
        // Implementasi logika tambahan untuk menambahkan komentar ke konten
        System.out.println("Komentar berhasil ditambahkan: " + komentar);
    }

    private static void tambahkanRating(Konten konten, double rating) {
        // Implementasi logika tambahan untuk memberikan rating ke konten
        System.out.println("Rating berhasil ditambahkan: " + rating);
    }
}
