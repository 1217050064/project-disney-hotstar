# NO. 1
Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)
Dalam program DisneyHotstarApp, terdapat beberapa pendekatan matematika dan algoritma pemrograman yang dapat diidentifikasi:

1. Pendekatan Matematika:
    
    - Menghitung total durasi tontonan pengguna:
        - Menggunakan konsep penjumlahan untuk menambahkan durasi tontonan secara berurutan.
        - Menggunakan variabel totalDurasi untuk menyimpan hasil penjumlahan.
    ![Gif1.1](https://gitlab.com/1217050064/project-disney-hotstar/-/raw/main/gif1.1.gif)

2. Algoritma Pemrograman:
    - Melakukan login dan validasi:
        - Menggunakan variabel loginBerhasil untuk menyimpan hasil login.
        - Menggunakan variabel MAX_LOGIN_ATTEMPTS sebagai batas maksimal percobaan login.
        - Menggunakan pernyataan if untuk memeriksa apakah login berhasil atau tidak.
        ![Gif1.2.1](https://gitlab.com/1217050064/project-disney-hotstar/-/raw/main/gif1.2.1.gif)
    - Menampilkan menu dan memproses pilihan pengguna:
        - Menggunakan pernyataan switch-case untuk memproses pilihan pengguna.
        - Menggunakan variabel pilihan untuk menyimpan pilihan pengguna.
        - Memanggil metode-metode yang sesuai berdasarkan pilihan pengguna.
        ![Gif1.2.2](https://gitlab.com/1217050064/project-disney-hotstar/-/raw/main/gif1.2.2.gif)
    - Menambahkan komentar:
        - Menggunakan metode tambahkanKomentar() dengan parameter konten dan komentar.
        - Menggunakan scanner untuk mendapatkan input pengguna.
        - Melakukan validasi nomor konten dan rating.
        ![Gif1.2.3](https://gitlab.com/1217050064/project-disney-hotstar/-/raw/main/gif1.2.3.gif)
    - Memberikan rating:
        - Menggunakan metode tambahkanRating() dengan parameter konten dan rating.
        - Menggunakan scanner untuk mendapatkan input pengguna.
        - Melakukan validasi nomor konten dan rating.
        - Pendekatan matematika dan algoritma pemrograman tersebut digunakan dalam beberapa bagian program untuk melakukan perhitungan, validasi, dan pengambilan keputusan berdasarkan input pengguna. Pendekatan matematika melibatkan konsep dasar matematika seperti penjumlahan, sedangkan algoritma pemrograman menerjemahkan langkah-langkah logis menjadi instruksi yang dapat dijalankan oleh komputer.
        ![Gif1.2.4](https://gitlab.com/1217050064/project-disney-hotstar/-/raw/main/gif1.2.4.gif)

# NO. 2
Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

Jawaban :

Algoritma yang dijelaskan sebelumnya untuk menghitung total durasi tontonan pengguna dalam konteks project Disney dapat dijelaskan sebagai berikut:

1. Dapatkan daftar tontonan pengguna dari objek Pengguna.
    - Algoritma ini membutuhkan akses ke daftar tontonan pengguna yang disimpan dalam variabel daftarTontonan.
2. Inisialisasi variabel totalDurasi dengan nilai 0.
    - Variabel totalDurasi akan digunakan untuk menjumlahkan durasi setiap konten dalam daftar tontonan pengguna.
3. Lakukan perulangan untuk setiap konten dalam daftar tontonan pengguna.
    - Algoritma ini menggunakan perulangan untuk mengakses setiap konten dalam daftar tontonan pengguna.
4. Dapatkan durasi konten saat ini.
    - Algoritma ini mengakses durasi konten saat ini dengan memanggil metode getDurasi() dari objek konten yang sedang diproses dalam perulangan.
5. Tambahkan durasi konten ke totalDurasi.
    - Algoritma ini menjumlahkan durasi konten saat ini ke totalDurasi menggunakan operator penjumlahan (+=).
6. Setelah selesai perulangan, totalDurasi akan berisi jumlah total durasi tontonan pengguna.
- Algoritma ini akan menghasilkan totalDurasi yang merupakan hasil penjumlahan durasi dari setiap konten dalam daftar tontonan pengguna.

Dengan menggunakan algoritma di atas, kita dapat menghitung total durasi tontonan pengguna dengan menjumlahkan durasi dari setiap konten dalam daftar tontonan. Algoritma tersebut menggambarkan pendekatan matematika sederhana dan menggunakan algoritma pemrograman berupa perulangan untuk mengakses dan menjumlahkan durasi dari setiap konten.

![Gif2](https://gitlab.com/1217050064/project-disney-hotstar/-/raw/main/gif2.gif)
# NO. 3
Mampu menjelaskan konsep dasar OOP

Jawaban :
Berikut adalah konsep dasar OOP yang diterapkan:

Enkapsulasi: Enkapsulasi melibatkan pengelompokan data dan metode terkait dalam suatu kelas, serta pengaturan aksesibilitas dari data dan metode tersebut. Dalam program ini, konsep enkapsulasi terlihat dengan penggunaan kata kunci private untuk mengenkapsulasi atribut username, password, dan daftarTontonan dalam kelas Pengguna. Atribut-atribut ini hanya dapat diakses melalui metode-metode publik yang ditentukan dalam kelas tersebut. Hal ini membantu mencegah akses langsung ke data dan memastikan bahwa data hanya dapat diakses dan dimodifikasi melalui metode-metode yang ditentukan.

Pewarisan (Inheritance): Pewarisan melibatkan membuat kelas baru (kelas turunan) yang mengambil atau mewarisi atribut dan metode dari kelas yang sudah ada (kelas induk atau superclass). Dalam program ini, terdapat pewarisan yang ditunjukkan oleh kelas AcaraTV dan Film yang merupakan subkelas dari kelas Konten. Subkelas ini mewarisi atribut dan metode dari kelas Konten. Misalnya, kelas AcaraTV dan Film menggunakan atribut dan metode yang didefinisikan dalam kelas Konten, seperti judul, durasi, dan tampilkanInfo(). Pewarisan membantu menghindari duplikasi kode, memungkinkan pemodelan hierarki, dan memfasilitasi reusabilitas kode.

Polimorfisme: Polimorfisme adalah konsep yang memungkinkan penggunaan objek dengan tipe yang sama, namun berperilaku berbeda sesuai dengan tipe aktualnya. Dalam program ini, polimorfisme terlihat ketika objek acaraTV dan film yang merupakan instance dari kelas turunan (AcaraTV dan Film) dimasukkan ke dalam objek tampilanKonten yang memiliki tipe kelas induk (Konten). Melalui polimorfisme, metode tampilkanInfo() yang dipanggil pada objek tampilanKonten dapat mengeksekusi implementasi yang sesuai di kelas turunan masing-masing. Polimorfisme memungkinkan penggunaan fleksibel dan penggantian objek dengan objek lain yang memiliki perilaku yang sama.

Abstraksi: Abstraksi melibatkan penyorotan dan pemodelan aspek penting dari objek dan menyembunyikan detail yang tidak relevan atau kompleks. Dalam program ini, kelas abstrak Konten digunakan untuk mengabstraksi konsep konten yang dapat ditonton secara umum. Kelas ini memiliki atribut dan metode yang relevan untuk semua jenis konten, seperti judul, durasi, dan tampilkanInfo(). Namun, kelas Konten tidak dapat diinstansiasi langsung karena merupakan kerangka kerja untuk kelas turunannya. Konsep abstraksi memungkinkan pemodelan yang lebih tinggi tingkat dan menyederhanakan kompleksitas sistem.

Dengan menerapkan keempat pilar OOP ini, program di atas menjadi lebih terstruktur, modular, dan dapat diubah dengan mudah. Setiap kelas dan objek memiliki tanggung jawab dan perilaku yang terdefinisi dengan jelas, dan konsep OOP membantu dalam pengembangan, pemeliharaan, dan pengelolaan kode yang lebih baik.

![gif3](https://gitlab.com/1217050064/project-disney-hotstar/-/raw/main/gif3.gif)

# NO. 4
Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

![gif4](https://gitlab.com/1217050064/project-disney-hotstar/-/raw/main/gif4.gif)

# NO. 5
Mampu mendemonstrasikan penggunaan Abstraction secara tepat  (Lampirkan link source code terkait)

![gif5](https://gitlab.com/1217050064/project-disney-hotstar/-/raw/main/gif5.gif)

# NO. 6
Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

![gif6](https://gitlab.com/1217050064/project-disney-hotstar/-/raw/main/gif6.gif)

# NO. 7
Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

Untuk menerjemahkan proses bisnis atau kumpulan use case ke dalam skema OOP, berikut adalah langkah-langkah yang dapat diikuti:

1. Identifikasi entitas utama:

- Pengguna: Representasi pengguna aplikasi Disney+ Hotstar dengan atribut username, password, dan daftar tontonan.
- Konten: Representasi konten yang dapat ditonton seperti acara TV dan film dengan atribut judul, durasi, dan informasi tambahan terkait jenis konten.
- Berlangganan: Representasi berlangganan pengguna dengan atribut status berlangganan dan metode untuk mengaktifkan atau menonaktifkan berlangganan.
- Komentar: Representasi komentar pengguna terhadap suatu konten dengan atribut isi komentar dan metode untuk menambahkan komentar.
- Rating: Representasi rating pengguna terhadap suatu konten dengan atribut nilai rating dan metode untuk memberikan rating.

2. Definisikan kelas-kelas:

- Kelas Pengguna: Mewakili pengguna dengan atribut username, password, dan daftar tontonan. Memiliki metode untuk login pengguna, menambahkan konten ke daftar tontonan, dan lainnya.
Kelas Konten: Mewakili konten yang dapat ditonton dengan atribut judul, durasi, dan informasi tambahan terkait jenis konten. Berisi metode abstrak tampilkanInfo() yang diimplementasikan oleh kelas-kelas turunannya.
- Kelas AcaraTV: Turunan kelas Konten, menambahkan atribut jumlahEpisode dan mengimplementasikan metode tampilkanInfo().
- Kelas Film: Turunan kelas Konten, menambahkan atribut sutradara dan mengimplementasikan metode tampilkanInfo().
- Kelas Berlangganan: Mewakili status berlangganan pengguna dengan atribut status berlangganan dan metode untuk mengaktifkan atau menonaktifkan berlangganan.
- Kelas Komentar: Mewakili komentar pengguna dengan atribut isi komentar dan metode untuk menambahkan komentar.
- Kelas Rating: Mewakili rating pengguna dengan atribut nilai rating dan metode untuk memberikan rating.

3. Tentukan relasi antar kelas:

- Kelas Pengguna memiliki komposisi dengan Kelas Berlangganan, yaitu objek Berlangganan menjadi bagian dari objek Pengguna.
- Kelas Pengguna memiliki asosiasi dengan Kelas Konten, yaitu pengguna memiliki daftar tontonan yang terdiri dari objek Konten.
- Kelas Konten memiliki asosiasi dengan Kelas Komentar dan Kelas Rating, yaitu objek Konten dapat memiliki objek Komentar dan objek Rating terkait.

4. Implementasikan metode fungsionalitas:

- Kelas Pengguna memiliki metode untuk login, menambahkan konten ke daftar tontonan, dan lainnya.
- Kelas Berlangganan memiliki metode untuk mengaktifkan dan menonaktifkan berlangganan.
- Kelas Komentar memiliki metode untuk menambahkan komentar.
- Kelas Rating memiliki metode untuk memberikan rating.

5. Gunakan objek untuk menjalankan proses bisnis:

- Dalam program utama atau driver, buat objek dari kelas-kelas yang sudah dibuat seperti objek Pengguna, objek Konten (AcaraTV dan Film), objek Berlangganan, objek Komentar, dan objek Rating.
- Gunakan metode-metode yang tersedia pada objek-objek tersebut untuk menjalankan proses bisnis atau use case yang diinginkan. Misalnya, objek Pengguna dapat melakukan login, menambahkan konten ke daftar tontonan, mengaktifkan atau menonaktifkan berlangganan, memberikan komentar, dan memberikan rating.

Dengan pendekatan ini, proses bisnis atau kumpulan use case dapat diorganisir dengan baik dalam struktur OOP yang jelas. Setiap entitas memiliki kelas yang terkait dan metode-metode yang relevan, sehingga memudahkan pengelolaan, perubahan, dan pengembangan aplikasi secara sistematis.

# NO. 8
Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

![ClassDiagram](https://gitlab.com/1217050064/project-disney-hotstar/-/raw/main/DisneyPBO.jpg)

Use Case Table:

| Use Case | Aktor | Deskripsi |
|----------|-------|-----------|
|Login|Pengguna|Proses login pengguna ke akun mereka di Disney+ Hotstar.|
|Aktifkan Berlangganan|Pengguna|Proses pengguna mengaktifkan berlangganan untuk mendapatkan akses ke konten premium di Disney+ Hotstar.|
|Nonaktifkan Berlangganan|Pengguna|Proses pengguna menonaktifkan berlangganan yang sudah ada di Disney+ Hotstar.|
|Memutar Konten|Pengguna|Proses pengguna memutar konten yang ada di Disney+ Hotstar.|
|Tambahkan Komentar|Pengguna|Proses pengguna menambahkan komentar pada konten yang mereka tonton di Disney+ Hotstar.|
|Beri Rating|Pengguna|Proses pengguna memberikan rating pada konten yang mereka tonton di Disney+ Hotstar.|
|Tampilkan Semua Konten|Pengguna|Proses pengguna melihat daftar semua konten yang tersedia di Disney+ Hotstar.|
|Logout|Pengguna|Proses pengguna logout dari akun mereka di Disney+ Hotstar.|

Login:

Aktor: Pengguna
Deskripsi: Use case ini menggambarkan proses di mana pengguna melakukan login ke akun mereka di Disney+ Hotstar.
Aksi: Pengguna memasukkan username dan password mereka.
Prekondisi: Pengguna belum berhasil login.
Postkondisi: Pengguna berhasil login ke akun mereka.

Aktifkan Berlangganan:

Aktor: Pengguna
Deskripsi: Use case ini menggambarkan proses di mana pengguna mengaktifkan berlangganan untuk mendapatkan akses ke konten premium di Disney+ Hotstar.
Aksi: Pengguna memilih opsi untuk mengaktifkan berlangganan.
Prekondisi: Berlangganan belum aktif.
Postkondisi: Berlangganan berhasil diaktifkan.

Nonaktifkan Berlangganan:

Aktor: Pengguna
Deskripsi: Use case ini menggambarkan proses di mana pengguna menonaktifkan berlangganan yang sudah ada di Disney+ Hotstar.
Aksi: Pengguna memilih opsi untuk menonaktifkan berlangganan.
Prekondisi: Berlangganan aktif.
Postkondisi: Berlangganan berhasil dinonaktifkan.

Memutar Konten:

Aktor: Pengguna
Deskripsi: Use case ini menggambarkan proses di mana pengguna memutar konten yang ada di Disney+ Hotstar.
Aksi: Pengguna memilih konten yang ingin diputar.
Prekondisi: Konten tersedia.
Postkondisi: Konten sedang diputar.

Tambahkan Komentar:

Aktor: Pengguna
Deskripsi: Use case ini menggambarkan proses di mana pengguna menambahkan komentar pada konten yang mereka tonton di Disney+ Hotstar.
Aksi: Pengguna memasukkan komentar pada konten.
Prekondisi: Pengguna telah menonton konten.
Postkondisi: Komentar berhasil ditambahkan pada konten.

Beri Rating:

Aktor: Pengguna
Deskripsi: Use case ini menggambarkan proses di mana pengguna memberikan rating pada konten yang mereka tonton di Disney+ Hotstar.
Aksi: Pengguna memberikan rating pada konten.
Prekondisi: Pengguna telah menonton konten.
Postkondisi: Rating berhasil diberikan pada konten.

Tampilkan Semua Konten:

Aktor: Pengguna
Deskripsi: Use case ini menggambarkan proses di mana pengguna melihat daftar semua konten yang tersedia di Disney+ Hotstar.
Aksi: Pengguna memilih opsi untuk melihat semua konten.
Prekondisi: Tidak ada.
Postkondisi: Daftar semua konten ditampilkan kepada pengguna.

Logout:

Aktor: Pengguna
Deskripsi: Use case ini menggambarkan proses di mana pengguna logout dari akun mereka di Disney+ Hotstar.
Aksi: Pengguna memilih opsi untuk logout.
Prekondisi: Pengguna telah login.
Postkondisi: Pengguna berhasil logout dari akun mereka.

# NO. 9
Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

!(https://youtu.be/dUEksrUf_co)


# NO. 10
Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

![UX](https://gitlab.com/1217050064/project-disney-hotstar/-/raw/main/UXDisney.jpg)
