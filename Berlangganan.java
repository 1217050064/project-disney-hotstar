public class Berlangganan {
    private boolean berlanggananAktif;

    public Berlangganan() {
        this.berlanggananAktif = false;
    }

    public void aktifkanBerlangganan() {
        this.berlanggananAktif = true;
        System.out.println("Berlangganan berhasil diaktifkan!");
    }

    public void nonaktifkanBerlangganan() {
        this.berlanggananAktif = false;
        System.out.println("Berlangganan berhasil dinonaktifkan!");
    }

    public boolean isBerlanggananAktif() {
        return berlanggananAktif;
    }
}
