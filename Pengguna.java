import java.util.*;

public class Pengguna {
    private String username;
    private String password;
    private List<Konten> daftarTontonan;
    private Berlangganan berlangganan;

    public Pengguna(String username, String password) {
        this.username = username;
        this.password = password;
        this.daftarTontonan = new ArrayList<>();
        this.berlangganan = new Berlangganan();
    }

    public void aktifkanBerlangganan() {
        berlangganan.aktifkanBerlangganan();
    }

    public void nonaktifkanBerlangganan() {
        berlangganan.nonaktifkanBerlangganan();
    }

    public boolean isBerlanggananAktif() {
        return berlangganan.isBerlanggananAktif();
    }

    // Metode untuk menambahkan konten ke daftar tontonan pengguna
    public void tambahkanTontonan(Konten konten) {
        daftarTontonan.add(konten);
    }

    // Metode untuk mendapatkan daftar tontonan pengguna
    public List<Konten> getDaftarTontonan() {
        return daftarTontonan;
    }

    public int hitungTotalDurasiTontonan() {
        int totalDurasi = 0;
        for (Konten konten : daftarTontonan) {
            totalDurasi += konten.getDurasi();
        }
        return totalDurasi;
    }
}
