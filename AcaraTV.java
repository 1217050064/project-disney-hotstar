public class AcaraTV extends Konten {
    private int jumlahEpisode;

    public AcaraTV(String judul, int durasi, int jumlahEpisode) {
        super(judul, durasi);
        this.jumlahEpisode = jumlahEpisode;
    }

    public int getJumlahEpisode() {
        return jumlahEpisode;
    }

    @Override
    public void putar() {
        VideoPlayer player = new VideoPlayer();
        player.putarVideo(this);
    }
}