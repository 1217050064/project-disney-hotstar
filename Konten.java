import java.util.*;

public abstract class Konten {
    private String judul;
    private int durasi;
    private List<String> komentar;
    private double rating;

    public Konten(String judul, int durasi) {
        this.judul = judul;
        this.durasi = durasi;
        this.komentar = new ArrayList<>();
        this.rating = 0;
    }

    public String getJudul() {
        return judul;
    }

    public int getDurasi() {
        return durasi;
    }

    public void tambahkanKomentar(String komentar) {
        this.komentar.add(komentar);
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getRating() {
        return rating;
    }

    public void tampilkanInfo() {
        System.out.println("Judul: " + getJudul());
        System.out.println("Durasi: " + getDurasi() + " menit");
        System.out.println("Rating: " + getRating());
        System.out.println("Komentar: ");
        for (String komentar : komentar) {
            System.out.println("- " + komentar);
        }
        System.out.println();
    }

    public abstract void putar();
}
