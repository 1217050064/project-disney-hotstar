import java.util.ArrayList;
import java.util.List;

public class TampilanKonten {
    private List<Konten> daftarKonten;

    public TampilanKonten() {
        this.daftarKonten = new ArrayList<>();
    }

    public void tambahkanKonten(Konten konten) {
        daftarKonten.add(konten);
    }

    public List<Konten> getDaftarKonten() {
        return daftarKonten;
    }

    public void tampilkanSemuaKonten() {
        int nomorKonten = 1;
        for (Konten konten : daftarKonten) {
            System.out.print(nomorKonten + ". ");
            konten.tampilkanInfo();
            nomorKonten++;
        }
    }
}
