public class Film extends Konten {
    private String sutradara;

    public Film(String judul, int durasi, String sutradara) {
        super(judul, durasi);
        this.sutradara = sutradara;
    }

    public String getSutradara() {
        return sutradara;
    }

    @Override
    public void putar() {
        VideoPlayer player = new VideoPlayer();
        player.putarVideo(this);
    }
}